import { aesCreateCipher, aesCreateDecipher } from "https://deno.land/x/notranspile_aes@1.0.0/mod.ts";
import * as b64 from "https://deno.land/std@0.146.0/encoding/base64.ts";
import { printf } from "https://deno.land/std@0.146.0/fmt/printf.ts";
import "https://deno.land/std@0.146.0/dotenv/load.ts";

if (Deno.args.length < 2) {
    Deno.exit(-1);
}

const encoder = new TextEncoder();
const decoder = new TextDecoder();
const key = encoder.encode(Deno.env.get("CON1CON2_KEY"));
const iv = encoder.encode(Deno.env.get("CON1CON2_IV"));

function strencrypt(plaintext: string): string | null {
    const bytes = encoder.encode(plaintext);
    let encrypted: Uint8Array;
    try {
        const cipher = aesCreateCipher("CBC", key, iv);
        cipher.update(bytes);
        encrypted = cipher.finish();
    }
    catch (err) {
        console.log(err);
        return null;
    }
    return b64.encode(encrypted);
}

function strdecrypt(ciphered: string): string | null {
    let bytes: Uint8Array;
    try {
        const encrypted = b64.decode(ciphered);
        const decipher = aesCreateDecipher("CBC", key, iv);
        decipher.update(encrypted);
        bytes = decipher.finish();
    }
    catch (err) {
        console.log(err);
        return null;
    }
    return decoder.decode(bytes);
}

if (Deno.args[0].startsWith('e')) {
    const ciphered = strencrypt(Deno.args[1]);
    if (ciphered) {
        printf("%.*s\n", ciphered.length, ciphered);
    }
} else {
    const plaintext = strdecrypt(Deno.args[1]);
    if (plaintext) {
        printf("%.*s\n", plaintext.length, plaintext);
    }
}
